#!/bin/sh
NOW=$(date -Is | tr -d '[\-:+]')
mkdir -p backups/${NOW}

echo "== Backing up docker-compose.yml"
cp docker-compose.yml backups/${NOW}/

echo "== Backing up Bamboo Home"
docker-compose exec -T bamboo \
  tar --directory /var/atlassian/bamboo --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/bamboo-home.tar.gz

echo "== Backing up Bamboo Install"
docker-compose exec -T bamboo \
  tar --directory /opt/bamboo --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/bamboo-install.tar.gz

echo "== Backing up Bamboo database contents"
docker-compose exec -T postgresql-bamboo \
  pg_dump -U bamboo bamboodb \
  | gzip -c > backups/${NOW}/bamboo.sql.gz

echo "== Backing up Bitbucket Home"
docker-compose exec -T bitbucket \
  tar --directory /var/atlassian/bitbucket --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/bitbucket-home.tar.gz

echo "== Backing up Bitbucket Install"
docker-compose exec -T bitbucket \
  tar --directory /opt/bitbucket --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/bitbucket-install.tar.gz

echo "== Backing up Bitbucket database contents"
docker-compose exec -T postgresql-bitbucket \
  pg_dump -U bitbucket bitbucketdb \
  | gzip -c > backups/${NOW}/bitbucket.sql.gz

echo "== Backing up Confluence Home"
docker-compose exec -T confluence \
  tar --directory /var/atlassian/confluence --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/confluence-home.tar.gz

echo "== Backing up Confluence Install"
docker-compose exec -T confluence \
  tar --directory /opt/atlassian/confluence --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/confluence-install.tar.gz

echo "== Backing up Confluence database contents"
docker-compose exec -T postgresql-confluence \
  pg_dump -U confluence confluencedb \
  | gzip -c > backups/${NOW}/confluence.sql.gz

echo "== Backing up Jira Home"
docker-compose exec -T jira \
  tar --directory /var/atlassian/jira --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/jira-home.tar.gz

echo "== Backing up Jira Install"
docker-compose exec -T jira \
  tar --directory /opt/jira --exclude='*logs' --create --gzip --file - . \
  > backups/${NOW}/jira-install.tar.gz

echo "== Backing up Jira database contents"
docker-compose exec -T postgresql-jira \
  pg_dump -U jira jiradb \
  | gzip -c > backups/${NOW}/jira.sql.gz

echo "== Making a super tarball"
tar --remove-files --create --verbose --file backups/tulsa-backup-${NOW}.tar backups/${NOW}/
