#!/bin/bash

if (( $# != 1 )); then
  echo "Parameter required: the date-stamp directory name you wish to restore, eg:"
  ( cd backups && ls -d * )
  exit 1
fi

NOW=$(date -Is | tr -d '[\-:+]')
TIMESTAMP=$1

echo "== Restoring docker-compose.yml"
cp backups/${TIMESTAMP}/docker-compose.yml ./

echo "== Moving existing Confluence Home files out of the way"
docker-compose exec -T confluence \
  mkdir -p /var/atlassian/confluence_pre_${NOW}

docker-compose exec -T confluence sh -c \
  "mv /var/atlassian/confluence/* /var/atlassian/confluence_pre_${NOW}/"

docker-compose exec -T confluence sh -c \
  "rm -rf /var/atlassian/confluence/*"

docker-compose exec -T confluence \
  chown confluence:confluence /var/atlassian/confluence 

echo "== Restoring backup ${TIMESTAMP} Confluence Home directory"
cat backups/${TIMESTAMP}/confluence-home.tar.gz \
  | docker-compose exec -T confluence tar --directory /var/atlassian/confluence -zxf -

echo "== Moving existing Confluence Install files out of the way"
docker-compose exec -T confluence \
  mv /opt/atlassian/confluence /opt/atlassian/confluence_pre_${NOW}

docker-compose exec -T confluence \
  mkdir /opt/atlassian/confluence 

docker-compose exec -T confluence \
  chown confluence:confluence /opt/atlassian/confluence 

echo "== Stopping Confluence"
docker-compose kill confluence
echo "== Waiting for Confluence to stop"
sleep 10

echo "== Restoring backup ${TIMESTAMP} Confluence Install directory"
cat backups/${TIMESTAMP}/confluence-install.tar.gz \
  | docker-compose exec -T confluence tar --directory /opt/atlassian/confluence -zxf -

echo "== Moving existing Confluence database out of the way"
echo "ALTER DATABASE confluencedb RENAME TO confluencedb_pre_${NOW}" \
  | docker-compose exec -T postgresql-confluence \
  psql -U postgres
  
echo "== Restoring backup ${TIMESTAMP} Confluence database"
echo "CREATE DATABASE confluencedb" \
  | docker-compose exec -T postgresql-confluence \
  psql -U postgres
  
echo 'GRANT ALL PRIVILEGES ON DATABASE "confluencedb" to confluence;' \
  | docker-compose exec -T postgresql-confluence \
  psql -U postgres
  
cat backups/${TIMESTAMP}/confluence.sql.gz | gzip -dc \
  | docker-compose exec -T postgresql-confluence \
  psql -U confluence confluencedb

echo "== Restarting Confluence and HTTPD"
docker-compose up -d confluence
sleep 2
docker-compose kill httpd
sleep 2
docker-compose up -d httpd
